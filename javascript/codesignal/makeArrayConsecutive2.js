function makeArrayConsecutive2(statues) {
    statues.sort()
    let n = statues.length;
    let last = Math.max(...statues)
    let first = Math.min(...statues)
    return (last - first + 1) - n;
}


console.log(makeArrayConsecutive2([-3, 2, 3, 8]))
console.log(makeArrayConsecutive2([4,5,7,9]))
